<?php
include_once("../includes/header.php");
if (empty($_POST['fname'])) {
    echo '<div style="text-align: center;" class="alert-info alert-block">';
    echo '<a class="close" data-dismiss="alert">×</a>';
    echo 'Info: Once an employee has been created, add the user account.';
    echo '</div>';
}
if (!empty($_POST['fname'])) {
    try {
        // check if exists        
        $query = "Select count(*) from employee where fname='".$_POST['fname']."' and lname='".$_POST['lname']."' and address='".$_POST['address']."'";
        $result = $conn->prepare($query); 
        $result->execute(); 
        $counter = $result->fetchColumn();

        if ($counter > 0) {
            echo '<div style="text-align: center;" class="alert-error alert-block">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo 'Error: Employee has already been added to the system.';
            echo '</div>';
        } else {
            // build sql insert statement from posted values
            $date = date("Y-m-d");
            $sql1 = "INSERT INTO Employee (fname, lname, department, address, city, state, zip, phone, hire_date) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $q = $conn->prepare($sql1);
            $q->execute(array($_POST['fname'], $_POST['lname'], $_POST['department'], $_POST['address'], $_POST['city'], $_POST['state'], $_POST['zip'], $_POST['phone'], $date));
            // Print results
            echo '<div style="text-align: center;" class="alert-success alert-block">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo 'Success: Employee was added successfully!';
            echo '</div>';

            // clear post values so we don't resubmit
            unset($_POST);
            unset($counter);
        }
    }
    catch(PDOException $e)
    {
        echo $e->getMessage();
    }
}
?>
<head>
    <meta charset="UTF-8">
    <title>Secure Login: Registration Form</title>
    <script type="text/JavaScript" src="../js/sha512.js"></script> 
    <script type="text/JavaScript" src="../js/forms.js"></script>
    <link rel="stylesheet" href="../css/main.css" />
</head>
<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add Employee</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">                           
                <div class="col-lg-6">
                <form method="post" name="registration_form">
                           <div class="panel panel-default">
                        <div class="panel-heading">                                            
                            Enter Employee Information
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                        <div class="row">
                                            <div class="col-lg-8" style="padding-left: 30px; padding-right: 30px;" >
                                                <div class="form-group control-group">
                                                    <label>First Name</label>
                                                    <div class="controls">                                                   
                                                    <input type="text" id="fname" class="form-control" name="fname" required/>
                                                    </div>
                                                </div>
                                                <div class="form-group control-group">
                                                    <label>Last Name</label>
                                                    <div class="controls">                                                   
                                                    <input type="text" id="lname" class="form-control" name="lname" required/>
                                                    </div>
                                                </div>
                                                <div class="form-group control-group">
                                                    <label>Department</label>
                                                    <div class="controls">
                                                        <select class="form-control" id="department" name="department">
                                                        <option value="Accounting">Accounting</option>
                                                        <option value="Finance">Finance</option>
                                                        <option value="Parts">Parts</option>
                                                        <option value="Sales">Sales</option>
                                                        <option value="Service">Service</option> 
                                                    </select>
                                                    </div>
                                                </div> 
                                                <div class="form-group control-group">
                                                    <label>Address</label>
                                                    <div class="controls">                                                   
                                                    <input type="text" id="address" class="form-control" name="address" required/>
                                                    </div>
                                                </div>
                                                <div class="form-group control-group">
                                                    <label>City</label>
                                                    <div class="controls">                                                   
                                                    <input type="text" id="city" class="form-control" name="city" required/>
                                                    </div>
                                                </div>
                                                <div class="form-group control-group">
                                                    <label>State</label>
                                                    <div class="controls">                                                   
                                                    <input type="text" id="state" class="form-control" name="state" required/>
                                                    </div>
                                                </div>
                                                <div class="form-group control-group">
                                                    <label>Zip</label>
                                                    <div class="controls">                                                   
                                                    <input type="text" id="zip" class="form-control" name="zip" required/>
                                                    </div>
                                                </div>
                                                <div class="form-group control-group">
                                                    <label>Phone</label>
                                                    <div class="controls">                                                   
                                                    <input type="text" id="phone" class="form-control" name="phone" required/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3" style="padding-left: 30px;">
                                                <button class="btn btn-primary" type="submit">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

<?php include_once("../includes/footer.php") ?>