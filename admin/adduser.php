<?php
include_once("../includes/header.php");
include_once '../includes/register.inc.php';
include_once '../includes/functions.php';
?>

<?php

// Most of this file was edited to conform to the EDMS
// Project, the SHA512 Hashing javascript was included from the below
// By Joel Miller


/* 
 * Copyright (C) 2013 peter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

include_once '../includes/connect_database.php';
$error_msg = "";

if (isset($_POST['empid'], $_POST['email'], $_POST['p'])) {
    // Sanitize and validate the data passed in
    $role = filter_input(INPUT_POST, 'role', FILTER_SANITIZE_STRING);
    $empid = filter_input(INPUT_POST, 'empid', FILTER_SANITIZE_STRING);
    $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
    $email = filter_var($email, FILTER_VALIDATE_EMAIL);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        // Not a valid email
        $error_msg .= '<p class="error">The email address you entered is not valid</p>';
    }
    
    $password = filter_input(INPUT_POST, 'p', FILTER_SANITIZE_STRING);
    if (strlen($password) != 128) {
        // The hashed pwd should be 128 characters long.
        // If it's not, something really odd has happened
        $error_msg .= '<p class="error">Invalid password configuration.</p>';
    }

    // Username validity and password validity have been checked client side.
    // This should should be adequate as nobody gains any advantage from
    // breaking these rules.
    //
    
    $prep_stmt = "SELECT id FROM user WHERE email = ? LIMIT 1";
    $stmt = $mysqli->prepare($prep_stmt);
    if ($stmt) {
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $stmt->store_result();
        
        if ($stmt->num_rows == 1) {
            // A user with this email address already exists
            $error_msg .= '<p class="error">A user with this email address already exists.</p>';
        }
    } else {
        $error_msg .= '<p class="error">Database error</p>';
    }

    // TODO: 
    // We'll also have to account for the situation where the user doesn't have
    // rights to do registration, by checking what type of user is attempting to
    // perform the operation.
    if (empty($error_msg)) {
        // Create a random salt
        $random_salt = hash('sha512', uniqid(openssl_random_pseudo_bytes(16), TRUE));

        // Create salted password 
        $password = hash('sha512', $password . $random_salt);
        $date = date("Y-m-d");
        // Insert the new user into the database 
        if ($insert_stmt = $mysqli->prepare("INSERT INTO user (employee_id, email, password, salt, role, last_modified) VALUES (?, ?, ?, ?, ?, ?)")) {
            $insert_stmt->bind_param('ssssss', $empid, $email, $password, $random_salt, $role, $date);
            // Execute the prepared query.
            if (! $insert_stmt->execute()) {
                header('Location: ../error.php?err=Registration failure: INSERT');
                exit();
            }
        }
            echo '<div style="text-align: center;" class="alert-success alert-block">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo 'Success: User has been added to the system.';
            echo '</div>';
    }
    } else {
            echo '<div style="text-align: center;" class="alert-info alert-block">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo 'Info: Only employees that have been added to the system will display here.';
            echo '</div>';
}
?>
<head>
    <meta charset="UTF-8">
    <title>Secure Login: Registration Form</title>
    <script type="text/JavaScript" src="../js/sha512.js"></script> 
    <script type="text/JavaScript" src="../js/forms.js"></script>
    <link rel="stylesheet" href="../css/main.css" />
</head>
<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add User</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">                           
                <div class="col-lg-4">
                <form method="post" name="registration_form">
                           <div class="panel panel-default">
                        <div class="panel-heading">                                            
                            Select New User
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                        <div class="row">
                                            <div class="col-lg-8" style="padding-left: 30px; padding-right: 30px;" >
                                                <div class="form-group control-group">
                                                    <label>Employee</label>
                                                    <div class="controls">                                                   
                                                    <select class="form-control" id="empid" name="empid">
                                                    <?php
                                                        $query = ("SELECT id, fname, lname FROM employee WHERE id NOT IN (SELECT employee_id FROM User)");
                                                        //$counter = $result -> rowCount();
                                                        foreach ($conn->query($query) as $row) {
                                                            echo "<option value=".$row['id'].">".$row['fname']." ".$row['lname']."</option>";
                                                        }
                                                    ?>
                                                    </select>
                                                    </div>
                                                </div>
                                                <div class="form-group control-group">
                                                    <label>Email</label>
                                                    <div class="controls">
                                                    <input type="text" id="email" class="form-control" name="email" required/>
                                                    </div>
                                                </div>
                                                <div class="form-group control-group">
                                                    <label>Password</label>
                                                    <div class="controls">
                                                    <input type="password" id="password" class="form-control" name="password" required/>
                                                    </div>
                                                </div>
                                                <div class="form-group control-group">
                                                    <label>Role</label>
                                                    <div class="controls">
                                                    <select class="form-control" id="role" name="role">
                                                    <option selected value="User">User</option>
                                                    <option value="Manager">Manager</option> 
                                                    </select>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                    <input type="button" 
                                                           value="Register" 
                                                           onclick="return regformhash(this.form,
                                                                           this.form.empid,
                                                                           this.form.password,
                                                                           this.form.role);" /> 
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

<?php include_once("../includes/footer.php") ?>
<script type="text/JavaScript" src="js/sha512.js"></script> 
<script type="text/JavaScript" src="js/forms.js"></script> 