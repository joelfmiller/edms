<?php
include_once("../includes/header.php");

if (!empty($_POST['cost'])) {

    try {
        // build sql insert statement from posted values
        $sql = "UPDATE Finance SET cost=? dealnotes=? WHERE VIN=? and status='pending'";
        $q = $conn->prepare($sql);
        $q->execute(array($_POST['cost'], $_POST['dealnotes'], $_POST['VIN']));

        // Print results
        echo '<div style="text-align: center;" class="alert-success alert-block">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo 'Success: Vehicle was updated successfully!';
        echo '</div>';
    }
    catch(PDOException $e)
    {
        echo $e->getMessage();
    }
        // clear post values so we don't resubmit
        unset($_POST);
        unset($counter);
}

if (!empty($_GET['id'])) {
    // build sql insert statement from posted values
    try {
        $query = "Select * from finance where id='".$_GET['id']."'";
        foreach($conn->query($query) as $row) {
            $customer_id = $row['customer_id'];
            $VIN = $row['VIN'];
            $notes = $row['dealnotes'];
            $cost = $row['cost'];
            if (!empty($row['VIN'])) {
                // build sql insert statement from posted values
                try {
                    $query = "Select * from vehicle where id='".$row['VIN']."'";
                    foreach($conn->query($query) as $row) {
                        $vehicle_cost = $row['cost'];
                        $vehicle_price = $row['price'];
                    }
                }
                catch(PDOException $e)
                {
                    echo $e->getMessage();
                }
            }
        }
    }
    catch(PDOException $e)
    {
        echo $e->getMessage();
    }
}
?>
<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Show Vehicle</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                <div class="col-lg-12">
                <form id="accountForm" method="post">
                    <div class="panel panel-default">
                        <div class="panel-heading">                                            
                            Vehicle Information
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                        <div class="row">
                                            <div class="col-lg-4" style="padding-left: 30px; padding-right: 30px;" >
                                                <div class="form-group control-group" >
                                                    <div class="controls">
                                                    <label>Finalize ?</label>
                                                        <input type="checkbox" name="finalize" value="finalize">
                                                    </div>
                                                </div>
                                                <div class="form-group control-group">
                                                    <label>VIN</label>
                                                    <div class="controls">                                                   
                                                    <input placeholder="1HGCR2F77EA005122" type="text" pattern="^[a-zA-Z0-9]{17}$" title="Must contain letters and numbers, 17 characters" <?php if(isset($VIN)){echo 'value="'.$VIN.'"'; }?> class="form-control" name="VIN_show" disabled required/>
                                                    <input hidden name="VIN" <?php if(isset($VIN)){echo 'value="'.$VIN.'"'; }?> />
                                                    </div>
                                                </div>
                                                <div class="form-group control-group">
                                                    <label>Vehicle Cost</label>
                                                    <div class="controls">
                                                    <input placeholder="99999" type="text" pattern="^[0-9]+$" title="Can only contain letters" <?php if(isset($vehicle_cost)){echo 'value="'.number_format($vehicle_cost).'"'; }?> class="form-control" name="vehicle_cost" disabled required/>
                                                    </div>
                                                </div>
                                                <div class="form-group control-group">
                                                    <label>Vehicle Price</label>
                                                    <div class="controls">
                                                    <input placeholder="99999" type="text" pattern="^[0-9]+$" title="Can only contain letters" <?php if(isset($vehicle_price)){echo 'value="'.number_format($vehicle_price).'"'; }?> class="form-control" name="vehicle_price" disabled required/>
                                                    </div>
                                                </div>
                                                <div class="form-group control-group">
                                                    <label>Deal Price</label>
                                                    <div class="controls">
                                                    <input placeholder="99999" type="text" pattern="^[0-9.]+$" title="Can only contain letters" <?php if(isset($cost)){echo 'value="'.$cost.'"'; }?> class="form-control" name="cost" required/>
                                                    </div>
                                                </div>
                                                <div class="form-group control-group" >
                                                    <div class="controls">
                                                    <label>Deal Notes</label>
                                                    <textarea name="dealnotes" cols=60 class="form-control" rows="9" id="comment" required><?php if(isset($dealnotes)){echo $dealnotes; }?></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <button class="btn btn-primary" type="submit">Save</button>
                                                </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
<?php include_once("../includes/footer.php") ?>
<script>
$(document).ready(function() {
document.getElementById('salesnav').click();
});
$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
}); 
</script>
